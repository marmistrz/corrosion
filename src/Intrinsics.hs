{-# OPTIONS_GHC -Wall -Wno-name-shadowing #-}
module Intrinsics
    ( setupIntrinsics
    )
where

import           Control.Monad.Trans                      ( liftIO )

import           Interp.Types

iprintln :: Arglist -> Interpreter Value
iprintln [x] = do
    liftIO $ putStrLn $ valuePretty x
    return VUnit
iprintln _ = error "Internal error: println should accept exactly one argument"

setupIntrinsics :: Interpreter VEnv
setupIntrinsics = snd <$> createVar "println" (VFun $ Function iprintln)
