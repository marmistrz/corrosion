{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleContexts#-}
{-# OPTIONS_GHC -Wall -Wno-name-shadowing #-}

module Interp
    ( evalProgram
    , evalExp
    , evalDecls
    , evalFunctionByName
    )
where

import           Control.Monad.Except
import           Control.Monad.State
import           Data.Maybe                               ( fromMaybe )
import           Safe.Exact
import           Text.Show.Functions                      ( )

import           Parse.AbsCorrosion
import           Parse.PrintCorrosion

import           Interp.Types
import           Intrinsics                               ( setupIntrinsics )
import           Utils

evalFunctionByName :: ID -> Arglist -> Interpreter Value
evalFunctionByName id args =
    let evalFunction (Function fun) = fun
        handler err = throwError $ ("... in function " ++ id) : err
        evaluation = do
            funloc   <- getLoc id
            VFun fun <- getVar funloc
            evalFunction fun args
    in  catchError evaluation handler

evalProgram :: Program -> Interpreter ()
evalProgram (Prog decls) = do
    intrEnv <- setupIntrinsics
    env     <- withEnv intrEnv $ evalDecls decls
    unless (hasVar "main" env) $ throwError ["No main function defined"]
    _ <- withEnv env $ evalFunctionByName "main" []
    return ()

evalDecls :: [Decl] -> Interpreter VEnv
evalDecls =
    let evalfun (DFun ident args _ block) = evalDFun ident args block
    in  foldEnv evalfun

substVars :: [(ID, Value)] -> Interpreter VEnv
substVars = foldEnv (fmap snd . uncurry createVar)

evalDFun :: Ident -> [TypedArg] -> Block -> Interpreter VEnv
evalDFun (Ident id) args block
    = let
          unwrap (TypedArg (Ident id) _) = id
          zipargs a b =
              fromMaybe
                      (error
                          "Internal interpreter error:\
                    \Mismatched sizes of formal and actual arguments"
                      )
                  $ zipExactMay a b
      in
          do
              (loc, newenv) <- createVar id VUnit
              let untypedargs = map unwrap args
                  funreprw    = VFun $ Function funrepr
                  funrepr arglist = do
                      let zipped = zipargs untypedargs arglist
                      env <- withEnv newenv $ substVars zipped
                      withEnv env (evalBlock block)
              modify (setVar loc funreprw)
              return newenv

type BinOp a b = a -> a -> b
type Evaluator2 = Exp -> Exp -> Interpreter Value

evalBoolBinop :: (a -> Value) -> BinOp Bool a -> Evaluator2
evalBoolBinop constr op e1 e2 = do
    VBool r1 <- evalExp e1
    VBool r2 <- evalExp e2
    return . constr $ op r1 r2

evalIntBinop :: (a -> Value) -> BinOp Integer a -> Evaluator2
evalIntBinop constr op e1 e2 = do
    VInt r1 <- evalExp e1
    VInt r2 <- evalExp e2
    return . constr $ op r1 r2

evalCmp :: BinOp Integer Bool -> Evaluator2
evalCmp = evalIntBinop VBool

evalLogical :: BinOp Bool Bool -> Evaluator2
evalLogical = evalBoolBinop VBool

evalArithmetic :: BinOp Integer Integer -> Evaluator2
evalArithmetic = evalIntBinop VInt

evalBlock :: Block -> Interpreter Value
evalBlock x = case x of
    BlockSt stmts            -> evalBlock $ BlockExp stmts EUnit
    BlockExp []          exp -> evalExp exp
    BlockExp (stmt : xs) exp -> case stmt of
        SExpr e -> do
            _ <- evalExp e
            evalBlock $ BlockExp xs exp
        SLetTyped l i _ e -> evalBlock $ BlockExp (SLet l i e : xs) exp
        SLet _ (Ident ident) letexp -> do
            val      <- evalExp letexp
            (_, env) <- createVar ident val
            withEnv env (evalBlock $ BlockExp xs exp)
        SFn (DFun ident args _ block) -> do
            env <- evalDFun ident args block
            withEnv env (evalBlock $ BlockExp xs exp)

vrange :: Integer -> Integer -> [Value]
vrange a b =
    let irange = if a < b then [a, a + 1 .. b - 1] else [a, a - 1 .. b + 1]
    in  map VInt irange

forIn :: ID -> Block -> Iterator -> Interpreter ()
forIn ident bl it =
    let singleIter :: Value -> Interpreter Value
        singleIter val = do
            (_, env) <- createVar ident val
            withEnv env (evalBlock bl)
    in  mapM_ singleIter it

evalExp :: Exp -> Interpreter Value
evalExp x = case x of
    EIf exp block1 block2 -> do
        VBool b <- evalExp exp
        if b then evalBlock block1 else evalBlock block2
    ENeg exp -> do
        VBool b <- evalExp exp
        return $ VBool (not b)
    EOr      exp1 exp2 -> evalLogical (||) exp1 exp2
    EAnd     exp1 exp2 -> evalLogical (&&) exp1 exp2
    EEq      exp1 exp2 -> evalCmp (==) exp1 exp2
    ENeq     exp1 exp2 -> evalCmp (/=) exp1 exp2
    ELess    exp1 exp2 -> evalCmp (<) exp1 exp2
    ELE      exp1 exp2 -> evalCmp (<=) exp1 exp2
    EGreater exp1 exp2 -> evalCmp (>) exp1 exp2
    EGE      exp1 exp2 -> evalCmp (>=) exp1 exp2
    EAdd     exp1 exp2 -> evalArithmetic (+) exp1 exp2
    ESub     exp1 exp2 -> evalArithmetic (-) exp1 exp2
    EMul     exp1 exp2 -> evalArithmetic (*) exp1 exp2
    EDiv     exp1 exp2 -> do
        VInt r1 <- evalExp exp1
        VInt r2 <- evalExp exp2
        if r2 == 0
            then throwError
                ["... in expression " ++ printTree x, "Division by zero"]
            else return . VInt $ div r1 r2

    EInt    integer       -> return . VInt $ integer
    ERefMut ident         -> evalExp $ ERef ident
    ERef    (Ident ident) -> do
        loc <- getLoc ident
        return . VRef $ loc

    ECall (Ident ident) untypedargs -> do
        let unwrap (UntypedArg a) = a
        arglist <- mapM (evalExp . unwrap) untypedargs
        evalFunctionByName ident arglist
    EVar (Ident ident) -> do
        loc <- getLoc ident
        getVar loc

    EWhile exp block -> do
        VBool g <- evalExp exp
        if g then evalBlock block >> evalExp x else return VUnit

    EFor (Ident ident) exp1 exp2 block -> do
        VInt val1 <- evalExp exp1
        VInt val2 <- evalExp exp2
        let range = vrange val1 val2
        forIn ident block range
        return VUnit

    EBlock block -> evalBlock block
    EDeref exp   -> do
        VRef loc <- evalExp exp
        getVar loc

    EAss (Ident ident) exp -> do
        val <- evalExp exp
        loc <- getLoc ident
        modify (setVar loc val)
        return VUnit
    ERefAss lexp exp -> do
        val      <- evalExp exp
        VRef loc <- evalExp lexp
        modify (setVar loc val)
        return VUnit
    ETrue  -> return $ VBool True
    EFalse -> return $ VBool False
    EUnit  -> return VUnit
