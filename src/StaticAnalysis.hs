{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# OPTIONS_GHC -Wall -Wno-name-shadowing #-}

module StaticAnalysis
    ( runAnalyzer
    , typecheckProgram
    )
where

import           Control.Monad                            ( when )
import           Control.Monad.Except
import           Control.Monad.Reader
import qualified Data.Map                      as Map
import           Safe.Exact                               ( zipExactMay )

import           Parse.AbsCorrosion
import           StaticAnalysis.Error
import           Utils


newtype Mutability = Mutability Bool deriving Show

let2mut :: Let -> Mutability
let2mut Let    = Mutability False
let2mut LetMut = Mutability True

immut :: Mutability
immut = Mutability False

data VarProps = VarProps { mutable :: Mutability, ty :: Type } deriving Show
type Env = Map.Map Ident VarProps
type Analyzer a = ExceptT Error (Reader Env) a

throwErrorCase :: ErrorCase -> Analyzer a
throwErrorCase ecase = throwError $ Error Nothing ecase []

addBacktrace :: Ident -> Error -> Analyzer a
addBacktrace ident err =
    let bt = backtrace err in throwError $ err { backtrace = ident : bt }

runAnalyzer :: Analyzer a -> Either Error a
runAnalyzer ana = runReader (runExceptT ana) Map.empty

assertType :: Exp -> Type -> Analyzer ()
assertType expr expected = do
    etype <- typecheckExp expr
    assertTypeEq expected etype

assertTypeEq :: Type -> Type -> Analyzer ()
assertTypeEq exp act =
    let emsg = throwErrorCase Mismatch {expected = exp, actual = act}
    in  when (exp /= act) emsg

-- | Returns a new environment, containing a given variable
registerVar :: Ident -> Mutability -> Type -> Analyzer Env
registerVar ident mut ty =
    let props = VarProps mut ty in asks (Map.insert ident props)

getProps :: Ident -> Analyzer VarProps
getProps ident = do
    may <- asks (Map.lookup ident)
    case may of
        Nothing -> throwErrorCase $ UndefVar ident
        Just p  -> return p

getType :: Ident -> Analyzer Type
getType ident = ty <$> getProps ident

checkDerive :: Exp -> Type -> Type -> Analyzer Type
checkDerive e exptype rettype = do
    assertType e exptype
    return rettype

checkDerive2 :: Exp -> Exp -> Type -> Type -> Analyzer Type
checkDerive2 e1 e2 exptype rettype = do
    assertType e1 exptype
    assertType e2 exptype
    return rettype

-- | Converts a declaration to a TFn type
fntype :: Decl -> Type
fntype (DFun _ args rettype _) =
    let unwrap (TypedArg _ ty) = ty
        types = map unwrap args
    in  TFn types rettype

----------------------------------------------------------

typecheckProgram :: Program -> Analyzer ()
typecheckProgram (Prog decls) = do
    _ <- typecheckDecls decls
    return ()

typecheckDecls :: [Decl] -> Analyzer Env
typecheckDecls = foldEnv typecheckDFun

typecheckDFun :: Decl -> Analyzer Env
typecheckDFun decl@(DFun ident args rettype block) =
    let fnty = fntype decl
        registerArg (TypedArg id ty) = registerVar id immut ty
        evaluation = do
            env      <- registerVar ident immut fnty
            envInner <- withEnv env $ foldEnv registerArg args
            blockty  <- withEnv envInner $ typecheckBlock block
            assertTypeEq rettype blockty
            return env
    in  evaluation `catchError` addBacktrace ident

typecheckStmt :: Stmt -> Analyzer Env
typecheckStmt x = case x of
    SExpr e -> do
        _ <- typecheckExp e
        ask
    SLetTyped l ident expectedType letexp -> do
        ty <- typecheckExp letexp
        assertTypeEq expectedType ty
        registerVar ident (let2mut l) ty
    SLet l ident letexp -> do
        ty <- typecheckExp letexp
        registerVar ident (let2mut l) ty
    SFn dfun -> typecheckDFun dfun

typecheckBlock :: Block -> Analyzer Type
typecheckBlock x = case x of
    BlockSt stmts      -> typecheckBlock $ BlockExp stmts EUnit
    BlockExp stmts exp -> do
        env <- foldEnv typecheckStmt stmts
        withEnv env $ typecheckExp exp

typecheckExp :: Exp -> Analyzer Type
typecheckExp x = evaluation `catchError` recordExp
  where
    recordExp err = case expr err of
        Just _  -> throwError err
        Nothing -> throwError $ err { expr = Just x }
    evaluation = case x of
        EIf exp block1 block2 -> do
            assertType exp TBool
            t1 <- typecheckBlock block1
            t2 <- typecheckBlock block2
            if t1 /= t2
                then throwErrorCase $ IfArmsTypeMismatch t1 t2
                else return t1
        ENeg exp           -> checkDerive exp TBool TBool
        EOr      exp1 exp2 -> checkDerive2 exp1 exp2 TBool TBool
        EAnd     exp1 exp2 -> checkDerive2 exp1 exp2 TBool TBool
        EEq      exp1 exp2 -> checkDerive2 exp1 exp2 TInt TBool
        ENeq     exp1 exp2 -> checkDerive2 exp1 exp2 TInt TBool
        ELess    exp1 exp2 -> checkDerive2 exp1 exp2 TInt TBool
        ELE      exp1 exp2 -> checkDerive2 exp1 exp2 TInt TBool
        EGreater exp1 exp2 -> checkDerive2 exp1 exp2 TInt TBool
        EGE      exp1 exp2 -> checkDerive2 exp1 exp2 TInt TBool
        EAdd     exp1 exp2 -> checkDerive2 exp1 exp2 TInt TInt
        ESub     exp1 exp2 -> checkDerive2 exp1 exp2 TInt TInt
        EMul     exp1 exp2 -> checkDerive2 exp1 exp2 TInt TInt
        EDiv     exp1 exp2 -> checkDerive2 exp1 exp2 TInt TInt
        ERef    ident      -> TRef <$> getType ident
        ERefMut ident      -> do
            props <- getProps ident
            let Mutability mut = mutable props
                innertype      = ty props
            unless mut $ throwErrorCase (ImmutableMutref ident innertype)
            return $ TMutRef innertype
        ECall ident@(Ident "println") untypedargs -> case untypedargs of
            [UntypedArg e] -> do
                _ <- typecheckExp e
                return TUnit
            _ -> throwErrorCase WrongArgNum
                { name   = ident
                , expnum = 1
                , actnum = length untypedargs
                }
        ECall ident untypedargs -> do
            let unwrap (UntypedArg p) = p
                argexps = map unwrap untypedargs

            calledtype   <- getType ident
            (types, ret) <- case calledtype of
                TFn types ret -> return (types, ret)
                _             -> throwErrorCase $ CallNonFn ident calledtype

            zipped <- case zipExactMay argexps types of
                Just z  -> return z
                Nothing -> throwErrorCase WrongArgNum
                    { name   = ident
                    , actnum = length argexps
                    , expnum = length types
                    }

            mapM_ (uncurry assertType) zipped
            return ret

        EVar ident       -> getType ident
        EWhile exp block -> do
            assertType exp            TBool
            assertType (EBlock block) TUnit
            return TUnit
        EFor ident exp1 exp2 block -> do
            assertType exp1 TInt
            assertType exp2 TInt
            env <- registerVar ident immut TInt
            withEnv env $ assertType (EBlock block) TUnit
            return TUnit
        EBlock block -> typecheckBlock block
        EDeref exp   -> do
            t <- typecheckExp exp
            case t of
                TRef    u -> return u
                TMutRef u -> return u
                _         -> throwErrorCase $ DerefNonRef t
        EAss ident exp -> do
            props <- getProps ident
            let ltype          = ty props
                Mutability mut = mutable props
            unless mut (throwErrorCase $ ImmutableAssign ident)
            assertType exp ltype
            return TUnit
        ERefAss lexp exp -> do
            ltype    <- typecheckExp lexp
            basetype <- case ltype of
                TRef    _ -> throwErrorCase $ ImmutableRefassign lexp ltype
                TMutRef u -> return u
                _         -> throwErrorCase $ DerefNonRef ltype
            assertType exp basetype
            return TUnit
        ETrue  -> return TBool
        EFalse -> return TBool
        EUnit  -> return TUnit
        EInt _ -> return TInt
