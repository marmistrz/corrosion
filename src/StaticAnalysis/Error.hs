{-# LANGUAGE NamedFieldPuns #-}
{-# OPTIONS_GHC -Wall -Wno-name-shadowing #-}

module StaticAnalysis.Error
    ( showError
    , Error(..)
    , ErrorCase(..)
    )
where

import           Parse.AbsCorrosion
import           Parse.PrintCorrosion

data Error = Error { expr :: Maybe Exp, errorcase :: ErrorCase, backtrace :: [Ident] }
data ErrorCase =
    Mismatch { expected :: Type, actual :: Type } |
    UndefVar { name :: Ident } |
    CallNonFn { name :: Ident, namety :: Type } |
    WrongArgNum { name :: Ident, expnum :: Int, actnum :: Int } |
    IfArmsTypeMismatch { first :: Type, second :: Type } |
    DerefNonRef { acttype :: Type } |
    ImmutableAssign { name :: Ident } |
    ImmutableRefassign { expression :: Exp, reftype :: Type} |
    ImmutableMutref { name :: Ident, reftype :: Type }
    deriving Show

showError :: Error -> String
showError Error { expr, errorcase, backtrace } =
    let formatBT (Ident ident) = "... in function: " ++ ident ++ "\n"
        intro = case expr of
            Just e  -> "Error in expression: " ++ printTree e ++ "\nReason: "
            Nothing -> "Error: "
    in  intro
        ++ formatAnalysisError errorcase
        ++ "\n"
        ++ foldMap formatBT backtrace

formatAnalysisError :: ErrorCase -> String
formatAnalysisError Mismatch { expected, actual } =
    "Type mismatch encountered\n"
        ++ "Expected type: "
        ++ printTree expected
        ++ "\n"
        ++ "Actual type: "
        ++ printTree actual

formatAnalysisError UndefVar { name } =
    "Undefined variable access\n" ++ "Variable name: " ++ printTree name

formatAnalysisError CallNonFn { name, namety } =
    "Attempt to call a non-callable variable\n"
        ++ "Variable name: "
        ++ (printTree name)
        ++ ", with type: "
        ++ printTree namety

formatAnalysisError WrongArgNum { name, actnum, expnum } =
    "Wrong number of arguments passed\n"
        ++ "to functional variable: "
        ++ (printTree name)
        ++ "expected number: "
        ++ show expnum
        ++ "actual number: "
        ++ show actnum

formatAnalysisError IfArmsTypeMismatch { first, second } =
    "Type mismatch in the arms of an if expression:\n"
        ++ "Type of the first arm: "
        ++ printTree first
        ++ "Type of the second arm: "
        ++ printTree second

formatAnalysisError DerefNonRef { acttype } =
    "Attempt to dereference a non-reference type, " ++ printTree acttype

formatAnalysisError ImmutableAssign { name } =
    "Attempt to assign to an immutable variable: " ++ printTree name

formatAnalysisError ImmutableMutref { name } =
    "Attempt to create a mutable reference from an immutable variable, "
        ++ printTree name

formatAnalysisError ImmutableRefassign { expression, reftype } =
    "Attempt to assign to a location pointed by an immutable reference\n"
        ++ "defined by the expression: "
        ++ printTree expression
        ++ "\n"
        ++ "of type: "
        ++ printTree reftype
