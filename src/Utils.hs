module Utils
    ( withEnv
    , foldApply
    , foldEnv
    )
where

import           Control.Monad.Reader

-- | Executes a computation in a predefined environment.
withEnv :: MonadReader r m => r -> m a -> m a
withEnv env = local (const env)

foldApply :: (Monad m) => (a -> m b) -> (b -> m b -> m b) -> m b -> [a] -> m b
foldApply _   _  base []       = base
foldApply fun wr base (p : xs) = do
    res <- fun p
    wr res $ foldApply fun wr base xs

foldEnv :: MonadReader r m => (a -> m r) -> [a] -> m r
foldEnv fun = foldApply fun withEnv ask
