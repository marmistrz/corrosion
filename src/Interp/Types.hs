{-# OPTIONS_GHC -Wall -Wno-name-shadowing #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Interp.Types
    ( Value(..)
    , Interpreter
    , ID
    , Loc
    , Iterator
    , Function(..)
    , VEnv
    , Store
    , Error
    , ProgState(..)
    , Arglist
    , runInterpreter
    , evalInterpreter
    , setVar
    , getVar
    , getLoc
    , createVar
    , valuePretty
    , hasVar
    )
where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State
import qualified Data.Map                      as Map
import           Data.Maybe                               ( fromMaybe )


type Interpreter a = ExceptT Error (StateT ProgState (ReaderT VEnv IO)) a

data Value =
    VInt Integer |
    VBool Bool |
    VFun Function |
    VRef Loc |
    VUnit
    deriving Show

valuePretty :: Value -> String
valuePretty x = case x of
    VInt  p -> show p
    VBool p -> show p
    VFun  p -> show p
    VRef  p -> "ref@" ++ show p
    VUnit   -> "()"

type ID = String
newtype Loc = Loc Integer deriving (Show, Ord, Eq, Num)
type Arglist = [Value]
type Iterator = [Value]
type Error = [String]

newtype Function = Function (Arglist -> Interpreter Value)
instance Show Function where
    show _ = "@FUNCTION@"

type VEnv = Map.Map ID Loc
type Store = Map.Map Loc Value

data ProgState = ProgState {
    store :: Store,
    freeloc :: Loc
} deriving Show

evalInterpreter :: Interpreter a -> IO (Either Error a)
evalInterpreter interp =
    let state  = runExceptT interp
        reader = evalStateT state (ProgState Map.empty $ Loc 0)
        bare   = runReaderT reader Map.empty
    in  bare

runInterpreter :: Interpreter a -> IO (Either Error a, ProgState)
runInterpreter interp =
    let state  = runExceptT interp
        reader = runStateT state (ProgState Map.empty $ Loc 0)
        bare   = runReaderT reader Map.empty
    in  bare



createVar :: ID -> Value -> Interpreter (Loc, VEnv)
createVar id val = do
    ProgState { store, freeloc } <- get
    let newstore = Map.insert freeloc val store
    newvenv <- asks (Map.insert id freeloc)
    put ProgState {store = newstore, freeloc = freeloc + 1}
    return (freeloc, newvenv)

setVar :: Loc -> Value -> ProgState -> ProgState
setVar loc val ps =
    let st    = store ps
        newst = Map.insert loc val st
    in  ps { store = newst }

getVar :: Loc -> Interpreter Value
getVar loc = do
    ProgState { store } <- get
    return $ fromMaybe (error $ "Uninhabited location: " ++ show loc)
                       (Map.lookup loc store)

getLoc :: ID -> Interpreter Loc
getLoc id =
    fromMaybe (error $ "Undefined variable: " ++ id) <$> asks (Map.lookup id)

hasVar :: ID -> VEnv -> Bool
hasVar = Map.member
