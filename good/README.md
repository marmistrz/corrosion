# Kompilacja
Dwie możliwości

## Z pomocą Stacka

```
stack build
```

Uruchamiamy jako `stack exec interpreter` (`stack exec` dodaje katalog z wynikowymi binarkami do `PATH`).
Można nawet użyć `stack exec $FAVORITE_SHELL`, żeby `interpreter` był dostępny w powłoce.

## Z pomocą Cabala
Plik `interpreter.cabal` został wygenerowany przez komendę `hpack`.

```
cabal install mtl safe HUnit
cabal configure
cabal build
```

Plik wykonywalny znajdzie się wówczas w
```
./dist/build/interpreter/interpreter
```
