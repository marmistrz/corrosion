#!/bin/bash

assertSucc() {
    if [ "$1" -ne 0 ]; then
        echo "The test failed, the output:"
        echo "$out"
        exit 1
    fi
}

for ff in good/*.out; do
    f="${ff%.*}"
    echo "Testing $f.corr"
    out=$(stack exec -- interpreter -v "$f".corr)
    assertSucc $?
    diff --unified <(echo "$out") <(cat "$f".out) || exit 1
done
