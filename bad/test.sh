#!/bin/bash

assertFail() {
    if [ ! "$1" -eq 1 ]; then
        echo "The test did not fail, aborting!"
        exit 1
    fi
}

for ff in bad/*.out; do
    f="${ff%.*}"
    echo "Testing $f.corr"
    out=$(stack exec -- interpreter -v "$f".corr)
    assertFail $?
    diff --unified <(echo "$out") <(cat "$f".out)
done

exit 0
