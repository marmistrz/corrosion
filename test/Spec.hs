import           Control.Monad                            ( when )
import           Control.Monad.Reader
import qualified Data.Map                      as Map
import           System.Exit                              ( exitFailure )
import qualified Test.HUnit                    as HUnit
import           Test.HUnit                               ( Test(TestCase)
                                                          , assertBool
                                                          , assertEqual
                                                          )

import           Parse.AbsCorrosion
import           Interp
import           Interp.Types

instance Eq Value where
    (==) a b = case (a, b) of
        (VInt p, VInt q) -> p == q
        (VBool p, VBool q) -> p == q
        (VUnit, VUnit) -> True
        (VFun _, VFun _) -> undefined
        _ -> False

instance Eq ProgState where
    (==) a b = store a == store b && freeloc a == freeloc b

interpTest
    :: Show a => (a -> Interpreter Value) -> a -> Either Error Value -> Test
interpTest ev exp val = TestCase $ do
    evv <- evalInterpreter $ ev exp
    assertEqual (show exp) evv val

interpTestState
    :: Show a
    => (a -> Interpreter Value)
    -> a
    -> (Either Error Value, ProgState)
    -> Test
interpTestState ev exp val = TestCase $ do
    evv <- runInterpreter $ ev exp
    assertEqual (show exp) evv val

-----------------------------------

arithmTestcase =
    let b = EInt 2
        c = EInt 3
        d = EInt 4
    in  EMul (EAdd c d) (EAdd b c)
arithmResult = return $ VInt 35

------------------------------------

letTestcase = EBlock $ BlockExp
    [ SLet Let (Ident "x") (EInt 21)
    , SLet Let (Ident "x") (EMul (EVar $ Ident "x") (EInt 2))
    ]
    (EVar (Ident "x"))
letResult = return $ VInt 42
letState = ProgState (Map.fromList [(0, VInt 21), (1, VInt 42)]) 2

------------------------------------

assignTC = EBlock $ BlockExp
    [SLet LetMut (Ident "x") (EInt 21), SExpr $ EAss (Ident "x") (EInt 42)]
    (EVar (Ident "x"))
assignR = return $ VInt 42
assignS = ProgState (Map.fromList [(0, VInt 42)]) 1

------------------------------------

forTC
    = let
          for = EFor
              (Ident "y")
              (EInt 1)
              (EInt 5)
              (BlockExp [] $ EAss
                  (Ident "x")
                  (EAdd (EVar (Ident "x")) (EVar (Ident "y")))
              )
      in  EBlock $ BlockExp [SLet LetMut (Ident "x") (EInt 0), SExpr for]
                            (EVar (Ident "x"))
forR = return $ VInt 10

------------------------------------

simpleFunTC =
    let decl  = DFun (Ident "bulba") [] TInt block
        block = BlockExp [] $ EInt 42
        exp   = ECall (Ident "bulba") []
    in  (decl, exp)
simpleFunEval :: (Decl, Exp) -> Interpreter Value
simpleFunEval (tc, exp) = do
    venv <- evalDecls [tc]
    local (const venv) (evalExp exp)
simpleFunR = return $ VInt 42

-------------------------------------

mulFunTC =
    let decl = DFun (Ident "bulba")
                    [TypedArg (Ident "x") TInt, TypedArg (Ident "y") TInt]
                    TInt
                    block
        ex    = EVar $ Ident "x"
        ey    = EVar $ Ident "y"
        block = BlockExp [] $ EIf (EGE ex ey) (BlockExp [] ex) (BlockExp [] ey)
        exp   = ECall (Ident "bulba") $ map (UntypedArg . EInt) [-4, 4]
    in  (decl, exp)
mulFunR = return $ VInt 4

expTests = HUnit.TestList
    [ interpTest evalExp arithmTestcase arithmResult
    , interpTestState evalExp letTestcase (letResult, letState)
    , interpTestState evalExp assignTC    (assignR  , assignS)
    , interpTest evalExp       forTC       forR
    , interpTest simpleFunEval simpleFunTC simpleFunR
    , interpTest simpleFunEval mulFunTC    mulFunR
    ]

main :: IO ()
main = do
    HUnit.Counts _ _ err fai <- HUnit.runTestTT expTests
    when (err > 0 || fai > 0) exitFailure
